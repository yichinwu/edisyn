from dd import cudd
import copy
import automaton as at
import operations as op
import bitvector as bv
import pdb
import time
import settings
import ast

class FileExtensionError(Exception):
    pass

class Game(object):
    
    def __init__(self, goodEdit, plant, kbound):
        self.prime = dict() #r,f ->r',f'
        self.unprime = dict() 
        self.bdd = cudd.BDD()
        self.vars = dict()         
        self.forall_trans = self.bdd.False
        self.exists_trans = self.bdd.False
        self.authentic_action = self.bdd.False
        self.insert_trans = self.bdd.False
        self.insert_action = self.bdd.False
        self.init = self.bdd.False
        self.kbound = kbound
        self._build(goodEdit, plant)

    def __str__(self):
        c = ['symbolic game structure:', 'vars = {v}','init = {init}','forall_trans = {forall}', 'exists_trans = {exists}', 'kbound = {k}']
        return '\n'.join(c).format(v = self.bdd.vars, init = self.init, forall = self.forall_trans, exists = self.exists_trans, k = self.kbound)

    def __del__(self):
        del self.forall_trans 
        del self.exists_trans
        del self.init
        del self.insert_trans
        del self.insert_action
        del self.authentic_action

    def _build(self, goodEdit, plant):
        self.plant_nvars = plant.vars['nstate']['bitnames']
        self.vars = copy.deepcopy(goodEdit.vars)
        self.vars.update({'out': dict(name = 'o', dom = self.vars['event']['dom'], width = self.vars['event']['width'], bitnames = list())})
        cudd.copy_vars(goodEdit.bdd, self.bdd)
        to_output = dict()
        for i, e in enumerate(self.vars['event']['bitnames']):
            self.vars['out']['bitnames'].append("o_{i}".format(i=i))
            self.bdd.add_var("o_{i}".format(i=i))
            to_output.update({self.vars['event']['bitnames'][i]:"o_{i}".format(i=i)}) 
        self.prime = goodEdit.prime
        self.unprime = goodEdit.unprime
        self.init = cudd.copy_bdd(goodEdit.init, goodEdit.bdd, self.bdd)
        
        #----exists trans: obfuscator actions----
        #(1)no operation: add authentic trans
        self.authentic_action = cudd.copy_bdd(goodEdit.trans, goodEdit.bdd, self.bdd)
        f1 = cudd.rename(self.authentic_action, self.bdd, to_output)
        self.exists_trans = self.bdd.apply('and', f1, self.authentic_action)
    
        
        #(2)replace operation
        f1 = cudd.copy_bdd(plant.trans, plant.bdd, self.bdd)
        f2 = cudd.copy_bdd(goodEdit.replace_trans, goodEdit.bdd, self.bdd)
        f2 = cudd.rename(f2, self.bdd, to_output)
        replace_action = self.bdd.apply('and', f1, f2)
        self.exists_trans = self.bdd.apply('or', self.exists_trans, replace_action)

        #(3)insert operation
        double_prime = dict()
        add_prime = dict()
        for s in goodEdit.vars['state']['bitnames']:
            self.bdd.add_var('nn'+s)
            double_prime.update({s:'nn'+s})
            add_prime.update({'n'+s:'nn'+s})
        evars = plant.vars['event']['bitnames']
        nnvars = double_prime.values()
        
        #TODO: the more elegant way to compute insertion strings
        """find insert post"""
        if self.kbound >= 1:
            self.insert_trans = cudd.copy_bdd(goodEdit.insert_trans, goodEdit.bdd, self.bdd)
            insert_trans1 = cudd.rename(self.insert_trans, self.bdd, add_prime)    
            insert_trans1 = self.bdd.quantify(insert_trans1, evars, False) #(r,f)->(r'',f'')
            insert_trans2 = cudd.rename(self.insert_trans, self.bdd, double_prime)
            insert_trans2 = self.bdd.quantify(insert_trans2, evars, False) # (r'',f'')->(r',f')
            z = insert_trans1 
            zold = None
            k = 1
            while z != zold and k < self.kbound:
                zold = z
                f = self.bdd.apply('and', zold, insert_trans2) #(r,f)->(r'',f'') & (r'',f'')->(r',f') 
                f = self.bdd.quantify(f, nnvars, False) #(r,f)->(r',f')
                f = cudd.rename(f, self.bdd, add_prime) #(r,f)->(r'',f'')
                z = self.bdd.apply('or', zold, f)
                k = k+1
            f = cudd.rename(self.authentic_action, self.bdd, double_prime) #(r'',f''), e, (r',f')    
            self.insert_action = z & f
            self.insert_action = self.bdd.quantify(self.insert_action, nnvars, False)
            self.exists_trans = self.bdd.apply('or', self.exists_trans, self.insert_action)
            #for insertion, output is left unconstrained
            del insert_trans1, insert_trans2, f, z

        #----forall trans: system actions----
        self.forall_trans = cudd.copy_bdd(plant.trans, plant.bdd, self.bdd)
        del f1, f2, replace_action

    
    def contains_tran(self, e, s, ns, trans):
        u = bv.tran_to_cube(e,s,ns,self.vars, self.bdd)
        return self.bdd.cofactor_function(trans, u) == self.bdd.True
        

def winning_set(game):
    """fixpoint computation of the winning set
    Args: 
        game (Game): game structure
    Returns:
        z (BDD): symbolic encoding of the winning positions
    """
    exists_vars = set(game.vars['out']['bitnames']) | set(game.vars['nstate']['bitnames']) - set(game.plant_nvars)
    forall_vars = set(game.vars['event']['bitnames']) | set(game.plant_nvars)
    z = game.bdd.True
    zold = None
    while z != zold:
        zold = z
        z = cudd.rename(z, game.bdd, game.prime)
        f = game.bdd.apply('and', z, game.exists_trans)
        f = game.bdd.apply('->', game.forall_trans, f)
        f = game.bdd.quantify(f, exists_vars, False)
        pre_z = game.bdd.quantify(f, forall_vars, True)
        z = game.bdd.apply('and', zold, pre_z)
    return z    

def print_function(bdd, aut, df, int_to_event, int_to_state):
    #f = self.insert_action
    #while f != bdd.False:
        #df = determinize(f, self.bdd)
    rvars = aut.vars['state']['bitnames'][0:len(aut.vars['state']['bitnames'])/2]
    fvars = aut.vars['state']['bitnames'][len(aut.vars['state']['bitnames'])/2:]
    rstate = int_to_state[(bv.bit_to_integer(df, rvars, bdd))]
    fstate = int_to_state[(bv.bit_to_integer(df, fvars, bdd))]
    nrvars = aut.vars['nstate']['bitnames'][0:len(aut.vars['state']['bitnames'])/2]
    nfvars = aut.vars['nstate']['bitnames'][len(aut.vars['state']['bitnames'])/2:]
    nrstate = int_to_state[(bv.bit_to_integer(df, nrvars, bdd))]
    nfstate = int_to_state[(bv.bit_to_integer(df, nfvars, bdd))]
    event = int_to_event[bv.bit_to_integer(df, aut.vars['event']['bitnames'], bdd)]
    print "({r};{f}),{e},({nr};{nf})".format(r = rstate, f = fstate, e = event,nr=nrstate, nf = nfstate)
        #f = f & ~bdd.quantify(df, self.bdd.vars-set(self.vars['state'])-set(self.vars['nstate']['bitnames'])-set(self.vars['event']['bitnames']), False)
 


def post_action(bdd, evar, s, game):
    """Computes the game transitions (or, the strategy) from given states
    Args:
        bdd (BDD): bdd manager of game
        evar (variable): event variables of game
        s (BDD): symbolic encoding of state (potentially multiple states)
        game (Game): game structure
    Returns:
        trans (BDD): symbolic encoding of transitions from state s
        is_insertion (list): flag indicating whether the returned action is inserted or not
    """
    trans = bdd.False
    insert_list = list()
    noninsert_list = list()
    for ev in xrange(1, evar['dom']):
        e = bv.var_to_cube(ev, evar, bdd)
        tran_for_ev = s & e & game.forall_trans
        if tran_for_ev == bdd.False:
            continue
        else: 
            tran_for_ev = tran_for_ev & game.exists_trans
            insert_tran_for_ev = tran_for_ev & game.insert_action
            if tran_for_ev == bdd.False:
                return bdd.False, insert_list, noninsert_list
        """extract a concrete tran from tran_for_ev for each state. For each state, select one action"""
        while tran_for_ev != game.bdd.False:
            t = tran_for_ev
            """determinize at one concrete starting position:"""
            for sbit in game.vars['state']['bitnames']:
                sb = game.bdd.var(sbit)
                u = t & sb
                if u == game.bdd.False: 
                    t = t & ~sb
                else:
                    t = u
            assert t != bdd.False
            tran_for_ev = tran_for_ev & ~t
            t = determinize(t, game.bdd)
            if insert_tran_for_ev & t != bdd.False:
                insert_list.append(t)
            else: noninsert_list.append(t)
            trans = trans | t
            del sb, t, u
    del e, tran_for_ev
    return trans, insert_list, noninsert_list



def post_nondet_action(bdd, evar, s, game):
    """Computes *all* game transitions (or, the maximally permissive strategy) from given states
    Args:
        bdd (BDD): bdd manager of game
        evar (variable): event variables of game
        s (BDD): symbolic encoding of state (potentially multiple states)
        game (Game): game structure
    Returns:
        trans (BDD): symbolic encoding of transitions from states s
        is_insertion (list): flag indicating whether the returned action is inserted or not
    """
    trans = bdd.False
    insert_list = list()
    noninsert_list = list()

    winning_tran = s & game.forall_trans
    winning_tran = winning_tran & game.exists_trans
    insert_winning_tran = winning_tran & game.insert_action
    if winning_tran == bdd.False:
        return bdd.False, insert_list, noninsert_list
    """extract every concrete tran from tran_for_ev"""
    t = bdd.False
    while winning_tran != game.bdd.False:
        t = determinize(winning_tran, game.bdd)
        winning_tran = winning_tran & ~t
        """pick only one action among all actions with the same (y, e, o, y') if not insertion, and (y,e,y') if insertion""" 
        if insert_winning_tran & t != bdd.False:
            insert_list.append(t)
            t = game.bdd.quantify(t, game.bdd.vars-set(game.vars['state']['bitnames'])-set(game.vars['nstate']['bitnames'])-set(game.vars['event']['bitnames']), False)
        else:
            noninsert_list.append(t)
            t = game.bdd.quantify(t, game.bdd.vars-set(game.vars['state']['bitnames'])-set(game.vars['nstate']['bitnames'])-set(game.vars['event']['bitnames'])-set(game.vars['out']['bitnames']), False)
        trans = trans | t
        #game.bdd.print_minterm(t)
        winning_tran = winning_tran & ~t
    del winning_tran, t
    return trans, insert_list, noninsert_list



def extract_explicit_obfuscator(game, winning_set, int_to_event, int_to_state, nondet):
    """extract explicit obfuscator (in edge list) from the game structure using the winning set
    Args:
        game (Game): the game of the synthesis problem
        winning_set (BDD): the symbolic set of winning game states
    Returns: 
        edge_list (list): the list of edges describing the obfuscator (mealy automaton)  
    """
    game.exists_trans = game.bdd.apply('and', game.exists_trans, winning_set)
    winning_set = cudd.rename(winning_set, game.bdd, game.prime)
    game.exists_trans = game.bdd.apply('and', game.exists_trans, winning_set) 
    """exists_trans is already restricted to winning strategies"""
    svars = game.vars['state']['bitnames']
    nsvars = game.vars['nstate']['bitnames']
    evars = game.vars['event']['bitnames']
    edge_list = list()

    s = game.init
    sold = game.bdd.False 
    while s != sold:
        sdiff = s & ~sold
        sold = s
        """f is the newly-found transitions
        f contains all necessary transitions; an action for every position and event.
        Hence, the while loop should extract every concrete tran from f
        """
        if nondet is False:
            f, insert_list, noninsert_list = post_action(game.bdd, game.vars['event'], sdiff, game) 
        else: 
            f, insert_list, noninsert_list = post_nondet_action(game.bdd, game.vars['event'], sdiff, game) 
        """update reached states"""
        reached = game.bdd.quantify(f, game.bdd.vars-set(nsvars), False)
        reached = cudd.rename(reached, game.bdd, game.unprime)
        s = sold | reached
        for df in insert_list:
            state = str(bv.bit_to_integer(df, svars, game.bdd))
            event = int_to_event[bv.bit_to_integer(df, evars, game.bdd)]
            out = compute_insert_output(df, game, int_to_event, int_to_state) + event
            nstate = str(bv.bit_to_integer(df, nsvars, game.bdd))
            edge_list.append((state, event, out, nstate))
        for df in noninsert_list:
            state = str(bv.bit_to_integer(df, svars, game.bdd))
            event = int_to_event[bv.bit_to_integer(df, evars, game.bdd)]
            out = int_to_event[bv.bit_to_integer(df, game.vars['out']['bitnames'], game.bdd)]
            nstate = str(bv.bit_to_integer(df, nsvars, game.bdd))
            edge_list.append((state, event, out, nstate))
    del s, sold, f, df, reached
    return edge_list
    


def compute_insert_output(f, game, int_to_event, int_to_state):
    """compute the concrete insertion string for insert_action f
    Args:
        f (BDD): symbolic encoding of "one" specific insertion action
        game (Game): game structure
    Returns:
        insertion (string): concrete insertion string 
    """
    svars = game.vars['state']['bitnames']
    nsvars = game.vars['nstate']['bitnames']
    evars = game.vars['event']['bitnames']
    #y: start state (r,f) of the input insert_action f
    y = game.bdd.quantify(f, game.bdd.vars-set(svars))
    #x: start state (r,f) with an outgoing transition labelled with the event of f
    x = game.bdd.quantify(f, svars) 
    x = game.authentic_action & x
    x = game.bdd.quantify(x, game.bdd.vars-set(svars))
    insert_list = list()
    #loop backward insertion reach until an overlapped state is found
    k = 0
    while x & y == game.bdd.False:
        nx = cudd.rename(x, game.bdd, game.prime)
        v = game.insert_trans & nx
        assert v != game.bdd.False #there must be an insertion path for f. hence v cannot be empty
        insert_list.append(v)
        v = game.bdd.quantify(v, game.bdd.vars-set(svars))
        x = v & ~x 
        k = k+1
    assert x&y == y
    insertion = ''
    ins_old = game.bdd.False
    s = y
    for ins in reversed(insert_list):
        ins = game.bdd.apply('and', ins, s)
        assert ins != game.bdd.False
        ins = determinize(ins, game.bdd)
        #print_function(game.bdd, game, ins, int_to_event, int_to_state)        
        s = game.bdd.quantify(ins, game.bdd.vars-set(nsvars))
        s = cudd.rename(s, game.bdd, game.unprime)
        insertion = insertion + ' ' + int_to_event[bv.bit_to_integer(ins, evars, game.bdd)]
    if insertion is not '':
        insertion = insertion + ' '
    return insertion 



def export_explicit_obfuscator(edge_list, out_file = 'obfuscator.txt', out_format = 'txt'):
    """ export the synthesized obfuscator to file. 
        Default format is edge list in .txt

    Args:
        edge_list (list): list of edges with (state, input, output, next_state) in concrete numerical names
        file_name (string): name of the file for the output obfuscator
    """
    formats = ['txt', 'fsm', 'json', 'dot']
    if out_format not in formats: 
        print 'invalid output format. Export edge list to default .txt format'
        to_txt(out_file, edge_list)
        
    else:
        if out_format == 'txt':
            to_txt(out_file, edge_list)
        elif out_format == 'fsm':
            to_fsm(out_file, edge_list)
        else: pass


def to_txt(out_file, edge_list):
    fout = open(out_file, 'w')
    fout.seek(0)
    for e in edge_list:
        edge = str(e)
        fout.write(edge+'\n')
    fout.close()


def to_fsm(out_file, edge_list):
    fout = open(out_file, 'w')
    fout.seek(0)
    state_table = dict()
    for s, e, o, ns in edge_list:
        elabel = '{inp}/{out}'.format(inp = e, out = o).replace(" ", "")
        if s not in state_table:
            state_table[s] = list()
            state_table[s].append((elabel, ns))
        else:
            if elabel not in state_table[s]:
                state_table[s].append((elabel,ns))
        if ns not in state_table:
            state_table[ns] = list()

    #output the initial state first
    fout.write(str(len(state_table)) + '\n')
    fout.write('\n0\t' + '1\t' + str(len(state_table['0'])) + '\n')
    for pair in state_table['0']:
        fout.write(pair[0] + '\t' + pair[1] + '\tc\to\n')

    for s in state_table:
        if s == '0':
            continue
        fout.write('\n' + s + '\t' + '1\t' + str(len(state_table[s])) + '\n')
        for pair in state_table[s]:
            fout.write(pair[0] + '\t' + pair[1] + '\tc\to\n')

def determinize(sym_tran, bdd):
    """Return one concrete tran from the BDD encoding of trans

    Args:
        sym_tran (BDD): BDD symbolic encoding of trans, possibly multiple concrete trans
        vars (dict): variables of the game
    Returns:
        concrete_tran (BDD): BDD encoding of one concrete tran
    """
    tran = bdd.pick_one_minterm(sym_tran)
    return tran

"""
start_time = 0

if __name__ == '__main__':
    settings.init()
    plant_file = "plant.aut"
    with open(plant_file, 'r') as fin:
        num_states, num_events = [int(x) for x in fin.readline().split()]
        t = fin.readline().strip()
        trans = ast.literal_eval(t)
        secret = eval(fin.readline().strip())
    plant = at.Automaton()
    plant.add_states(num_states, 1)
    plant.add_events(num_events)
    plant.build()
    plant.add_trans(trans)
    plant.add_secret(secret) 
    goodEdit = op.build_good_editFSM(plant, trans, "util.spc")  

    #print 'start building game: ' + str(time.time()-settings.get_start_time()) + ' sec'
    game = Game(goodEdit, plant)
    print 'start computing winning set: ' + str(time.time()-settings.get_start_time()) + ' sec'
    w = winning_set(game)
    if game.bdd.apply('and', game.init, w) != game.bdd.False:
        print 'start extracting obfuscator: ' + str(time.time()-settings.get_start_time()) + ' sec'
        edge_list = extract_explicit_obfuscator(game, w)
        if len(edge_list) != 0:
            export_explicit_obfuscator(edge_list, "obfuscator.aut")
        else: print "ERROR: none-empty winning set but zero winning edge"
    else: print "no winning obfuscator"
    del w, game
    print 'end of synthesis: ' + str(time.time()-settings.get_start_time())+ ' sec'"""
