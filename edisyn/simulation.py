import sys 
import os.path
import random

"""simulation based on the edit automaton"""
#how to write the interactive mode?

def random_trace(k, trans):
    print 'real_trace', 'obf_trace:'
    s = '0'
    while k >= 0:
        rand_ev = random.choice(trans[s].keys()) 
        o,s = random.choice(trans[s][rand_ev])
        print rand_ev, o
        k = k-1

def interactive(trans):
    """If the obfuscator is deterministic, then it will return the only output. 
    If the obfuscator is nondeterminsitic, then an output will be randomly selected. 
    """
    s = '0'
    while True:
        print 'state:',s
        print 'events:',trans[s].keys()
        e = raw_input("Enter the plant's output event:")
        if e is "q":
            return
        else:
            o,s = random.choice(trans[s][e])
            print "obfuscated output", o
        


def fsm_to_IO_aut(obf_file):
    """ Build edit automaton from the input .fsm file
    Args:
        obf_file (string): path to the .fsm file
    Returns:
        aut (edit automaton): explicit automaton
    """
    extension = os.path.splitext(obf_file)[1]
    if extension != '.fsm':
        raise FileExtensionError(extension)
    trans = dict() #{state1:{event: [output,next_state], ...}, state2:{event: [output,next_state], ...}, ...}
    with open(obf_file, 'r') as fin:
        num_states = int(fin.readline().strip())
        for s in range(num_states):
            fin.readline()
            state, public, num_trans = [x for x in fin.readline().strip().split()]
            if state not in trans:
                trans[state] = dict()
            for t in range(int(num_trans)):
                ev, nstate, c, o = [x for x in fin.readline().strip().split()]
                event, output = ev.split('/')
                if event not in trans[state]:
                    trans[state].update({event: list()})
                trans[state][event].append([output,nstate])
    return trans 




if __name__ == '__main__':
    if len(sys.argv) == 4 and sys.argv[1] == '-r':
        obf_file = sys.argv[3]
        k = int(sys.argv[2])
        trans = fsm_to_IO_aut(obf_file)
        random_trace(k, trans)    
    elif len(sys.argv) == 3 and sys.argv[1] == '-i': 
        obf_file = sys.argv[2]
        trans = fsm_to_IO_aut(obf_file)
        interactive(trans)
    else:
        print "Usage:" 
        print "Random simulation of k steps: python simulation.py -r <k> <obfuscator_file>"
        print "Interactive simulation: python simulation.py -i <obfuscator_file>"
        sys.exit(0)
