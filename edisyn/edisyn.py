from dd import cudd
import automaton as at
import operations as op
import bitvector as bv
import synthesis as syn
import pdb
import copy
import time
import os.path
import settings
import ast
import sys

start_time = 0

class FileExtensionError(Exception):
    pass

def fsm_to_aut(fsm_file):
    """ Build a symbolic.Automaton from the input .fsm file
    Args:
        fsm_file (string): path to the .fsm file
    Returns:
        aut (symbolic.Automaton): symbolic automaton model
        trans (dict): transition dict format for building aut
        state_to_int (dict): map from state names to integer indices
        event_to_int (dict): map from event names to integer indices. Index 0 is reserved for the epsilon event
    """ 
    extension = os.path.splitext(fsm_file)[1]
    if extension != '.fsm':
        raise FileExtensionError(extension)
    state_to_int = dict()
    int_to_state = dict()
    event_to_int = dict({'':0})
    int_to_event = dict({0:''})
    secrets = set()
    trans = dict()
    with open(fsm_file, 'r') as fin:
        num_states, num_events = [int(x) for x in fin.readline().strip().split()]
        for e in range(1,int(num_events)+1):
            trans[e] = dict()
        for s in range(num_states):
            fin.readline()
            state, public, num_trans = [x for x in fin.readline().strip().split()]
            if state not in state_to_int:
                sindex = len(state_to_int)
                state_to_int[state] = sindex
                int_to_state[sindex] = state
            if public == '0':
                secrets.add(state_to_int[state])
            for t in range(int(num_trans)):
                ev, nstate = [x for x in fin.readline().strip().split()]
                if ev not in event_to_int:
                    eindex = len(event_to_int)
                    event_to_int[ev] = eindex
                    int_to_event[eindex] = ev
                if nstate not in state_to_int:
                    sindex = len(state_to_int)
                    state_to_int[nstate] = sindex
                    int_to_state[sindex] = nstate
                if state_to_int[state] not in trans[event_to_int[ev]]:
                    trans[event_to_int[ev]].update({state_to_int[state]: list()})
                trans[event_to_int[ev]][state_to_int[state]].append(state_to_int[nstate])
    aut = _to_aut(num_states, num_events, trans, secrets)
    return aut, trans, state_to_int, event_to_int, int_to_state, int_to_event


def _to_aut(num_states, num_events, trans, secrets):
    aut = at.Automaton()
    aut.build(num_states, num_events)
    aut.add_trans(trans)
    aut.add_secret(secrets)
    return aut

def to_util_list(util_file, state_to_int):
    """ Build utility list from util_file that gives a list of valid (real,fake) state pairs 
    Args:
        util_file (string): path to the util file
        state_to_int (map): map from state names to indices
    Returns:
        util (list): list of (real,fake) state pairs in indices
    """
    if util_file is None:
        return None
    util = list()
    with open(util_file, 'r') as fin:
        for line in fin:
            pair = [state_to_int[x] for x in line.strip().split()]
            util.append(pair)
    return util

if __name__ == '__main__':
    if len(sys.argv) >= 4 and len(sys.argv) <= 6:
        settings.init()
        nondet = False
        if len(sys.argv) == 4:
            plant_file = sys.argv[1]
            util_file = sys.argv[2]
            out_file = sys.argv[3]
            kbound = sys.maxint
            out_format = "fsm" #os.path.splitext(out_file)[1].replace(".","")
        elif len(sys.argv) == 5 and sys.argv[1] == "-n":
            nondet = True
            plant_file = sys.argv[2]
            util_file = sys.argv[3]
            out_file = sys.argv[4]
            kbound = sys.maxint
            out_format = "fsm" #os.path.splitext(out_file)[2].replace(".","")
        elif len(sys.argv) == 6 and sys.argv[1] == "-b":
            kbound = int(sys.argv[2])
            plant_file = sys.argv[3]
            util_file = sys.argv[4]
            out_file = sys.argv[5]
            out_format = "fsm" #os.path.splitext(out_file)[3].replace(".","")
        else: sys.exit(0)
        try:
            plant, trans, state_to_int, event_to_int, int_to_state, int_to_event = fsm_to_aut(plant_file)
            util = to_util_list(util_file, state_to_int)
            """util.spc specifies the allowable state pairs"""
            goodEdit = op.build_good_editFSM(plant, trans, util)
            """build the game with insertion length bounded by k"""
            game = syn.Game(goodEdit, plant, kbound)
            w = syn.winning_set(game)
            if game.bdd.apply('and', game.init, ~w) == game.bdd.False:
                edge_list = syn.extract_explicit_obfuscator(game, w, int_to_event, int_to_state, nondet)
                if len(edge_list) != 0:
                    syn.export_explicit_obfuscator(edge_list, out_file, out_format)
                else: print "ERROR: none-empty winning set but zero winning edge"
                print game.bdd
            else: print "no winning obfuscator"
            del w, game
            t = str(time.time()-settings.get_start_time())
            print "end of synthesis: {t} sec".format(t = t)
        except FileExtensionError as e:
            print "incorrect file extension {e}".format(e = e)  

    else:
        print "Usage: "
        print "Standard deterministic synthesis: python editsyn.py <plant_fsm_file> <utility_file> <output_file>" 
        print "Deterministic synthesis with bounded insertion length k: python editsyn.py -b <k> <plant_fsm_file> <utility_file> <output_file>" 
        print "Nondeterministic synthesis: python editsyn.py -n <plant_fsm_file> <utility_file> <output_file>" 
        sys.exit(0) 
